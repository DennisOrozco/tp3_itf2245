#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "conf.h"
#include "common.h"
#include "vmm.h"
#include "tlb.h"
#include "pt.h"
#include "pm.h"

/*
* Tp # 3 IFT-2245
* Hiver-2018
* N'gaza Lazare Assoumou. matricule : 931020 
* Dennis Orozco Martinez. matricule : 20031060
*/

/*
 * Structure pour envoyer lors du traitement d'une adresse logique
 */
typedef struct 
{
  unsigned int page_number_s;
  unsigned int frame_s;
  unsigned int paddress_s;
  unsigned int offset_s;
  int frame_number_s;
} data;

static unsigned int read_count = 0;
static unsigned int write_count = 0;

static FILE* vmm_log;

void vmm_init (FILE *log)
{
  // Initialise le fichier de journal.
  vmm_log = log;
}


// NE PAS MODIFIER CETTE FONCTION
static void vmm_log_command (FILE *out, const char *command,
                             unsigned int laddress, /* Logical address. */
		             unsigned int page,
                             unsigned int frame,
                             unsigned int offset,
                             unsigned int paddress, /* Physical address.  */
		             char c) /* Caractère lu ou écrit.  */
{
  if (out)
    fprintf (out, "%s[%c]@%05d: p=%d, o=%d, f=%d pa=%d\n", command, c, laddress,
	     page, offset, frame, paddress);
}

/* Fonction pour decoder une adresse physique, retourne une struct avec les donnees
 * gere aussi les page faults en faisant appel aux methodes necessaires dans la TLB,
 * la table de pages et la memoire physique
 */
static data decode_data(unsigned int laddress)
{
  
  data data_dec;
  unsigned int
    page_number=0, offset=0, frame_number=0, padress=0;

  int n_bits = 0, tlb_value, tb_value;
  long addr_bin = decimalToBinary(laddress);
  char buffer [16];

  for (int i = 0; i < 16; i++){
     buffer[i] = 'c';
  }

  // compter le nombre de bits de l'adresse
  while(addr_bin > 0)
  {
    buffer [15 - n_bits] = addr_bin % 10;
    addr_bin = addr_bin / 10;
    n_bits ++;
  }
  
  if (n_bits >= 8) {    		// adresse avec plus de 8 bits
     for (int j = 0; j < 8; j++) {
       offset = offset * 10 + buffer [8 + j]; 
     }

     for (int k = 0; k < 8; k++){
        if (buffer [k] != 'c'){       
          page_number = page_number * 10 + buffer [k];
        }
     }    
     page_number = binaryToDecimal(page_number);

  } else { 				// adresse avec moin de 8 bits
  
    for (int j = 0; j < n_bits; j++) {
       offset = offset * 10 + buffer [16 - n_bits +j]; 
    }
  }

  tlb_value = tlb_lookup(page_number, 0);
  
  if (tlb_value == -1){		//TLB miss
     
      tb_value = pt_lookup(page_number);

      if(tb_value == -1){ 	// page_table miss, on ajoute le frame 
	  
	  frame_number = find_frame();

	  pm_download_page (page_number, frame_number);
    	  
	  pt_set_entry (page_number, frame_number);
         
	  tlb_add_entry(page_number, frame_number, true);

      } else { 			// page table hit -- ajouter dans tlb

	  frame_number = tb_value;
	  tlb_add_entry(page_number, frame_number, true);
	  pt_set_entry (page_number, frame_number);
      }

  } else { //TLB hit

    frame_number = tlb_value;
  }

  frame_number = decimalToBinary(frame_number);
  char buffer_fra[7];
  sprintf(buffer_fra, "%u", frame_number); 	//convertit frame(unsigned int) en frame string pour la concatenation
  
  char buff_str_of[7];
  sprintf(buff_str_of, "%u", offset); 	//convertit offset(unsigned int) en offset string pour la concatenation

  strcat(buffer_fra, buff_str_of);  		//concate frame et offset
  
  padress = strToUInt(buffer_fra);		//adresse physique = frame + offset

  data_dec.page_number_s = page_number;
  data_dec.frame_s = frame_number;
  data_dec.paddress_s = padress;
  data_dec.offset_s = offset;
  data_dec.frame_number_s = frame_number;
 
  return data_dec;
}


/*
 * fonction convertion decimal en binaire
 */
long decimalToBinary(long n)
{
  int remainder;
  long long binary = 0, i = 1;
  
  while(n != 0)
  {
    remainder = n % 2;
    n = n / 2;
    binary = binary + (remainder * i);
    i = i * 10;    
  }
  return binary;
}

/*
 * fonction convertion binaire en decimal
 */
int binaryToDecimal(long long n)
{
    int decimalNumber = 0, i = 0, remainder;
    while (n!=0)
    {
        remainder = n%10;
        n /= 10;
        decimalNumber += remainder * pow(2,i);
        ++i;
    }
    return decimalNumber;
}


// fonction convertion string en unsigned int
unsigned int strToUInt(char* str)
{
    unsigned int mult = 1;
    unsigned int re = 0;
    int len = strlen(str);
    int i;
    for(i = len -1 ; i >= 0 ; i--)
    {
        re = re + ((int)str[i] -48)*mult;
        mult = mult*10;
    }
    return re;
}


/* Effectue une lecture à l'adresse logique `laddress`.  */
char vmm_read (unsigned int laddress)
{
  char c = '!';
  data data_re = decode_data(laddress);

  vmm_log_command (stdout, "READING", laddress, data_re.page_number_s, data_re.frame_number_s, 
						data_re.offset_s, data_re.paddress_s, c);
  read_count ++;

  return c;
}

/* Effectue une écriture à l'adresse logique `laddress`.  */
void vmm_write (unsigned int laddress, char c)
{
  data data_wr = decode_data(laddress); 

  pm_write (data_wr.paddress_s, c);

  write_count++;
  vmm_log_command (stdout, "WRITING", laddress, data_wr.page_number_s, data_wr.frame_number_s, 
					            data_wr.offset_s, data_wr.paddress_s, c);
}


// NE PAS MODIFIER CETTE FONCTION
void vmm_clean (void)
{
  fprintf (stdout, "VM reads : %4u\n", read_count);
  fprintf (stdout, "VM writes: %4u\n", write_count);
}
