#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "conf.h"
#include "pm.h"

/*
* Tp # 3 IFT-2245
* Hiver-2018
* N'gaza Lazare Assoumou. matricule : 931020 
* Dennis Orozco Martinez. matricule : 20031060
*/

static FILE *pm_backing_store;
static FILE *pm_log;
static char pm_memory[PHYSICAL_MEMORY_SIZE];
static unsigned int download_count = 0;
static unsigned int backup_count = 0;
static unsigned int read_count = 0;
static unsigned int write_count = 0;

// Initialise la mémoire physique
void pm_init (FILE *backing_store, FILE *log)
{
  pm_backing_store = backing_store;
  pm_log = log;
  memset (pm_memory, '\0', sizeof (pm_memory));
}

// retourne une frame libre
unsigned int find_frame()
{
   unsigned int frame_number;
   for (int i = 0; i < 7936; i +=256){
	
       if (pm_memory[i] == ' ')
       {
	 frame_number = i/256;
	 return frame_number;
       }
    }
   
   return (rand()%32);
}

/*
 * fonction convertion binaire en decimal
 */
int binaryToDecimal_p(long long n)
{
    int decimalNumber = 0, i = 0, remainder;
    while (n!=0)
    {
        remainder = n%10;
        n /= 10;
        decimalNumber += remainder * pow(2,i);
        ++i;
    }
    return decimalNumber;
}



// Charge la page demandée du backing store
void pm_download_page (unsigned int page_number, unsigned int frame_number)

{

   char *buffer;
   FILE *fp;    
   bool finded = false;
   buffer = (char*)malloc(257);
   char * path_file;
   
   path_file = "/home/dennis/Documents/tp3-IFT2245/tp3_itf2245/tp3/tests/BACKING_STORE.txt";

   fp = fopen(path_file, "rb");
   if (fp == NULL)
   {
	fclose(fp);
	free(buffer);
	fprintf(stderr, "Unable to open file %s\n", "BACKING_STORE.txt");
	return;
   }

   fseek(fp, page_number, SEEK_SET);
   fread(buffer, sizeof(char), 256, fp);

   
   for (int i = frame_number; i < 256; i++){
      pm_memory[i] = buffer[i];
   }

  download_count++;
  
  fclose(fp);
  free(buffer);
}

// Sauvegarde la frame spécifiée dans la page du backing store
void pm_backup_page (unsigned int frame_number, unsigned int page_number)
{

  char *buffer;
  FILE *fp;    
  buffer = (char*)malloc(257);
  char *path_file = realpath("/BACKING_STORE.txt", NULL);
  //char * path_file = "/home/dennis/Documents/tp3-IFT2245/tp3_itf2245/tp3/tests/BACKING_STORE.txt";
 
  for (int i = frame_number; i < 256; i++){ // prends la frame dans la memoire physique et met dans le buffer
     buffer[i] = pm_memory[i];
  }   
 
  fp = fopen(path_file, "wb");
  
  if (fp == NULL)
  {
    fclose(fp);
    free(buffer);
    fprintf(stderr, "Unable to open file %s\n", "BACKING_STORE.txt");
    return;
  }

  fseek(fp, page_number, SEEK_SET);
  fwrite (buffer , sizeof(char), sizeof(buffer), fp); // mets le buffer dans backingStore
 
  fclose(fp);
  free(buffer);
  
  backup_count++;

}

char pm_read (unsigned int physical_address)
{
  
  read_count++;

  physical_address = binaryToDecimal_p(physical_address);
  
  return pm_memory[physical_address];
}

void pm_write (unsigned int physical_address, char c)
{

//aller dans le backingStore puis changer 
  
  write_count++;
  physical_address = binaryToDecimal_p(physical_address);

  pm_memory[physical_address] = c;
  
}


void pm_clean (void)
{
  // Enregistre l'état de la mémoire physique.
  if (pm_log)
    {
      for (unsigned int i = 0; i < PHYSICAL_MEMORY_SIZE; i++)
	{
	  if (i % 80 == 0)
	    fprintf (pm_log, "%c\n", pm_memory[i]);
	  else
	    fprintf (pm_log, "%c", pm_memory[i]);
	}
    }
  fprintf (stdout, "Page downloads: %2u\n", download_count);
  fprintf (stdout, "Page backups  : %2u\n", backup_count);
  fprintf (stdout, "PM reads : %4u\n", read_count);
  fprintf (stdout, "PM writes: %4u\n", write_count);
}
